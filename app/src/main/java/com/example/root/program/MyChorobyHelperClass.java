package com.example.root.program;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 28.10.16.
 */

public class MyChorobyHelperClass extends SQLiteOpenHelper {

    public static final String NAZWA_TABELI = "choroby";
    public static final String NAZWA_BAZY = "choroby.db";
    public static final int  WERSJA_BAZY = 1;
    public static final String CHOROBA = "choroba";
    public static final String OPIS = "opis";
    public static final String ID = "id";

    private Context context;
    private SQLiteDatabase db;
    private SQLiteOpenHelper myDatabaseHelperClass;


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+this.NAZWA_TABELI+
                "("+ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+CHOROBA+ " TEXT NOT NULL, "+OPIS+" TEXT);");


    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+this.NAZWA_BAZY);

        this.onCreate(db);

    }

    public MyChorobyHelperClass(Context context) {

        super(context,NAZWA_BAZY, null, 1);
        this.context = context;
    }



    public MyChorobyHelperClass openCommunication()
    {
        myDatabaseHelperClass = new MyChorobyHelperClass(context);

        try
        {
            db = myDatabaseHelperClass.getWritableDatabase();

        } catch(SQLException ex)
        {
            db = myDatabaseHelperClass.getReadableDatabase();
        }
        return this;
    }


    public void closeCommuication()
    {
        myDatabaseHelperClass.close();
    }


    public long insertTo(Choroba choroba)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MyChorobyHelperClass.CHOROBA,choroba.getNazwa());
        contentValues.put(MyChorobyHelperClass.OPIS,choroba.getOpis());
        return db.insert(MyChorobyHelperClass.NAZWA_TABELI,null,contentValues);
    }

    public int deleteTo(Choroba choroba)
    {

        return db.delete(NAZWA_TABELI, ID + " = ?",
                new String[] { String.valueOf(choroba.getId()) });

    }

    public Choroba getSelectedContact(int id)
    {
        String collumns[] = {MyChorobyHelperClass.ID, MyChorobyHelperClass.CHOROBA, MyChorobyHelperClass.OPIS};


        Cursor cursor = db.query(MyChorobyHelperClass.NAZWA_TABELI,collumns,ID+"=?",null,null,null,null);


        if(cursor !=null)
        {
            cursor.moveToFirst();
        }
        Choroba choroba = new Choroba(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2));
                return choroba;
    }




    public List<Choroba> getAllData()
    {

        String query = "SELECT * FROM "+NAZWA_TABELI;
        List<Choroba> chorobyLista = new ArrayList<Choroba>();
        Cursor cursor = db.rawQuery(query,null);
        boolean czyWyjdzie;
        if(cursor!=null && cursor.moveToFirst())
        {

            do {
                Choroba choroba = new Choroba();
                choroba.setId(Integer.parseInt(cursor.getString(0)));
                choroba.setNazwa(cursor.getString(1));
                choroba.setOpis(cursor.getString(2));
                chorobyLista.add(choroba);
                czyWyjdzie = cursor.moveToNext();

            }while(czyWyjdzie);



        }

        return chorobyLista;

    }


    public int getCountOfData()
    {
        String myQuery = "SELECT "+ID+","+CHOROBA+","+OPIS+" FROM "+this.NAZWA_TABELI;
        Cursor cursor = db.rawQuery(myQuery,null);
        return cursor.getCount();

    }

}
