package com.example.root.program;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static Button button_opis,button_choroba,button_dodatkowe,button_opcje;


    private static Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButton();
        context = getApplicationContext();


    }


    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.button_choroba:

                Intent intent = new Intent(this,ChorobaActivity.class);
                startActivity(intent);
                break;
            case R.id.button_dodatkowe:
                break;
            case R.id.button_opcje:
                Intent intentOpcje = new Intent(this,OpcjeActivity.class);
                startActivity(intentOpcje);
                break;
            case R.id.button_opis:

                Intent intentOpis = new Intent(this,OsobaActivity.class);
                startActivity(intentOpis);
                break;
            default:
                Toast.makeText(this,"Bląd !!",Toast.LENGTH_SHORT).show();

        }
    }


    /*

    @Desciption Inicjowanie zmiennych przyciskow okna dialogowego podstawowego(Kafle)
     */


    private void initButton()
    {
        button_opis = (Button) findViewById(R.id.button_opis);
        button_choroba = (Button) findViewById(R.id.button_choroba);
        button_dodatkowe = (Button) findViewById(R.id.button_dodatkowe);
        button_opcje = (Button) findViewById(R.id.button_opcje);

        button_choroba.setOnClickListener(this);
        button_opcje.setOnClickListener(this);
        button_opis.setOnClickListener(this);
        button_dodatkowe.setOnClickListener(this);
    }




}
