package com.example.root.program;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 08.11.16.
 */

public class MyOsobaHelperClass extends SQLiteOpenHelper {

    public static final String NAZWA_TABELI = "osoba";
    public static final String NAZWA_BAZY = "osoba.db";
    public static final int  WERSJA_BAZY = 1;
    public static final String CALE_IMIE = "CALE_IMIE";
    public static final String MIEJSCOWOSC = "miejscowosc";
    public static final String ID = "id";
    public  static final String ULICA = "ulica";
    public static final String KOD_POCZTOWY = "kodPocztowy";

    private Context context;
    private SQLiteDatabase db;
    private SQLiteOpenHelper myDatabaseHelperClass;

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+this.NAZWA_TABELI+
                "("+ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+CALE_IMIE+ " TEXT NOT NULL, "+MIEJSCOWOSC+" TEXT NOT NULL, "+ULICA+" TEXT NOT NULL, "+KOD_POCZTOWY+" TEXT NOT NULL);");


    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+this.NAZWA_BAZY);

        this.onCreate(db);

    }

    public MyOsobaHelperClass(Context context) {

        super(context,NAZWA_BAZY, null, 1);
        this.context = context;
    }



    public MyOsobaHelperClass openCommunication()
    {
        myDatabaseHelperClass = new MyOsobaHelperClass(context);

        try
        {
            db = myDatabaseHelperClass.getWritableDatabase();

        } catch(SQLException ex)
        {
            db = myDatabaseHelperClass.getReadableDatabase();
        }
        return this;
    }


    public void closeCommuication()
    {
        myDatabaseHelperClass.close();
    }






    public long insertTo(Osoba osoba)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MyOsobaHelperClass.MIEJSCOWOSC,osoba.getMiejoscowosc());
        contentValues.put(MyOsobaHelperClass.CALE_IMIE,osoba.getCaleImie());
        contentValues.put(MyOsobaHelperClass.KOD_POCZTOWY,osoba.getKodPocztowy());
        contentValues.put(MyOsobaHelperClass.ULICA,osoba.getUlica());
        return db.insert(MyChorobyHelperClass.NAZWA_TABELI,null,contentValues);
    }

    public int deleteTo(Osoba osoba)
    {

        return db.delete(NAZWA_TABELI, ID + " = ?",
                new String[] { String.valueOf(osoba.getId()) });

    }

    public Osoba getSelectedContact(int id)
    {
        String collumns[] = {MyOsobaHelperClass.ID, MyOsobaHelperClass.CALE_IMIE, MyOsobaHelperClass.MIEJSCOWOSC,MyOsobaHelperClass.ULICA,MyOsobaHelperClass.KOD_POCZTOWY};


        Cursor cursor = db.query(MyChorobyHelperClass.NAZWA_TABELI,collumns,ID+"=?",null,null,null,null);


        if(cursor !=null)
        {
            cursor.moveToFirst();
        }
        Osoba osoba = new Osoba(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4));
        return osoba;
    }




    public List<Osoba> getAllData()
    {

        String query = "SELECT * FROM "+NAZWA_TABELI;
        List<Osoba> osobaLista = new ArrayList<Osoba>();
        Cursor cursor = db.rawQuery(query,null);
        boolean czyWyjdzie;
        if(cursor!=null && cursor.moveToFirst())
        {

            do {
                Osoba osoba = new Osoba();
                osoba.setId(Integer.parseInt(cursor.getString(0)));
                osoba.setCaleImie(cursor.getString(1));
                osoba.setMiejoscowosc(cursor.getString(2));
                osobaLista.add(osoba);
                czyWyjdzie = cursor.moveToNext();

            }while(czyWyjdzie);



        }

        return osobaLista;

    }


    public int getCountOfData()
    {
        String myQuery = "SELECT "+ID+","+CALE_IMIE+","+MIEJSCOWOSC+","+ULICA+","+KOD_POCZTOWY+" FROM "+this.NAZWA_TABELI;
        Cursor cursor = db.rawQuery(myQuery,null);
        return cursor.getCount();

    }

}
