package com.example.root.program;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by root on 08.11.16.
 */

public class OsobaActivity extends Activity implements View.OnClickListener {

    static private Button button_zamknij;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.osoba_layout);
        initButton();
    }

    /*

    TODO
    Jak w ChorobyActivity


     */

    private void initButton()
    {
        button_zamknij = (Button) findViewById(R.id.zamknijPokazOsobe);
        button_zamknij.setOnClickListener(this);


        /*
        .....................
         */

    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.zamknijPokazOsobe:
                Intent mainIntent = new Intent(this,MainActivity.class);
                startActivity(mainIntent);
                finish();
                break;
        }

    }
}
