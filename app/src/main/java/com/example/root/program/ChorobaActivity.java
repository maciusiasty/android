package com.example.root.program;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by root on 01.11.16.
 */

public class ChorobaActivity extends Activity {


    private static Button button_zamknijDodajChorobe,button_DodajChorobe,button_doPrzodu,button_doTylu,button_usun;

    private static TextToSpeech textToSpeech;

    private static MyChorobyHelperClass myDatabaseHelperClass;


    private static EditText text_DodajChorobe,text_Dodajopis;

    private  static TextView text_opis,text_Chorobe;

    private ArrayList<String> nazwaChoroby = new ArrayList<String>();

    private ArrayList<String> opisChoroby = new ArrayList<String>();

    private static int ktoraChoroba,ilosc;

    private static List<Choroba> chorobaList = new ArrayList<Choroba>();

    private static Context context;

    //Przyciski z dialogu
    private static Button button_zamknij,button_synchronizacja,button_dodajChorobe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_choroby);


        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.ENGLISH);
                }
            }
        });


        myDatabaseHelperClass = new MyChorobyHelperClass(this);
        myDatabaseHelperClass.openCommunication();
        context = getApplicationContext();
        ktoraChoroba = 0;
        initButton();
        chorobaOblsuga(this);
    }


        @Override
        public void onPause(){
            if(textToSpeech !=null){
                textToSpeech.stop();
                textToSpeech.shutdown();
            }
            super.onPause();
        }



        @Override
        protected void onDestroy() {
            super.onDestroy();
            myDatabaseHelperClass.close();
        }


    /*
    @Description Inicjowanie przysciskow.
     */

    private void initButton() {

        button_doPrzodu = (Button) findViewById(R.id.wPrzod);
        button_doTylu = (Button) findViewById(R.id.wTyl);
        button_zamknij = (Button) findViewById(R.id.zamknij);
        button_synchronizacja = (Button) findViewById(R.id.synteraztor);
        button_dodajChorobe = (Button) findViewById(R.id.dodajChorobe);
        button_usun = (Button) findViewById(R.id.usun);
    }



    /*
    @Description Funkcja ktora wywoluje inne funkcje dotyczace dodawania choroby do bazy danych i ich ogladania (Kontener)
     */



    private  void chorobaOblsuga(final Context ctx)
    {


        odswiezEkran();

        button_usun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*
               TODO
               Choroba chorobaUsun = new Choroba(ktoraChoroba,null,null);
                int wynik = myDatabaseHelperClass.deleteTo(chorobaUsun);
                if(wynik == 0)
                    Toast.makeText(ctx,"Blad przy usuwaniu",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(ctx,"Usunieto rekord",Toast.LENGTH_SHORT).show();
                    */


            }
        });


        button_synchronizacja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                TODO
                Brak polskiego.
                 */
                String toSpeak1 = text_Chorobe.getText().toString()+". "+text_opis.getText().toString();
                textToSpeech.speak(toSpeak1,TextToSpeech.QUEUE_FLUSH,null);
            }
        });

        button_doPrzodu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ktoraChoroba<ilosc-1)
                    ktoraChoroba++;
                text_Chorobe.setText(chorobaList.get(ktoraChoroba).getNazwa());
                text_opis.setText(chorobaList.get(ktoraChoroba).getOpis());
                odswiezEkran();

            }
        });


        button_doTylu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ktoraChoroba>0)
                    ktoraChoroba--;
                text_Chorobe.setText(chorobaList.get(ktoraChoroba).getNazwa());
                text_opis.setText(chorobaList.get(ktoraChoroba).getOpis());
                odswiezEkran();

            }
        });


        dodajChorobe();

        button_zamknij.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(ctx,MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });


    }



    /*

    @Description Funkcja odswieza ekran przy ogladaniu chorob.

     */
     private void odswiezEkran()
    {

        text_Chorobe = (TextView) findViewById(R.id.nazwaChorobyAuto);
        text_opis = (TextView) findViewById(R.id.opisChorobyAuto);

        chorobaList = myDatabaseHelperClass.getAllData();

        ilosc = chorobaList.size();



        if (ilosc > 0
                ) {
            text_Chorobe.setText(chorobaList.get(ktoraChoroba).getNazwa());
            text_opis.setText(chorobaList.get(ktoraChoroba).getOpis());
        }


    }


    /*
    @Description Funkcja dodawania choroby do bazy danych.
     */

    private static void dodajChorobe()
    {



        button_dodajChorobe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                TODO
                Blad przy numeracji
                 */

                final Dialog dialogDodajChorobe = new Dialog(v.getContext());
                dialogDodajChorobe.setContentView(R.layout.layout_dodajchoroby);
                dialogDodajChorobe.setTitle("Dodaj chorobę");

                button_dodajChorobe = (Button) dialogDodajChorobe.findViewById(R.id.dodajChorobeClick);

                text_DodajChorobe = (EditText) dialogDodajChorobe.findViewById(R.id.nazwaChorobyDodaj);
                text_Dodajopis = (EditText) dialogDodajChorobe.findViewById(R.id.opisChorobyDodaj);


                button_dodajChorobe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Choroba choroba = new Choroba();
                        choroba.setOpis(text_Dodajopis.getText().toString());
                        choroba.setNazwa(text_DodajChorobe.getText().toString());

                        long czyUdalo  =  myDatabaseHelperClass.insertTo(choroba);
                        if(czyUdalo>=0)
                            Toast.makeText(dialogDodajChorobe.getContext(),"Dane zostaly dodane",Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(dialogDodajChorobe.getContext(),"Błąd bazy danych",Toast.LENGTH_SHORT).show();

                    }
                });



                button_zamknijDodajChorobe = (Button) dialogDodajChorobe.findViewById(R.id.zamknijDodajChorobe);
                button_zamknijDodajChorobe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDodajChorobe.dismiss();
                    }
                });




                dialogDodajChorobe.show();

            }
        });

    }

    }

