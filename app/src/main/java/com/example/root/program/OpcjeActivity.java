package com.example.root.program;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by root on 04.11.16.
 */



public class OpcjeActivity extends Activity implements View.OnClickListener{


    static private ArrayList<OpcjeListModel> listModel = new ArrayList<OpcjeListModel>();
    private static ListView listView;
    private static Button zamknijOpcje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opcjelista_layout);
        listModel.clear();
        initDataList();
        listView = (ListView) findViewById(R.id.listViewOpcje);
        MyListAdapter myListAdapter = new MyListAdapter(listModel,getApplicationContext());
        listView.setAdapter(myListAdapter);
        initButton();
        addOnClickListnerToListView();


    }


    /*
    @Descirption Dodawnie pozycji do listy
     */

    private void initDataList()
    {

        //Mozliwosc dodania zdjec po lewej strony tekstu!!!
        //TODO

        OpcjeListModel opcjeListModel1 = new OpcjeListModel();
        opcjeListModel1.setNazwa("Ustawienia glosnosci");
        opcjeListModel1.setImage(null);

        listModel.add(opcjeListModel1);
        OpcjeListModel opcjeListModel2 = new OpcjeListModel();

        opcjeListModel2.setImage(null);
        opcjeListModel2.setNazwa("Rozmiar kafelkow");

        listModel.add(opcjeListModel2);

    }

    private void initButton()
    {
      zamknijOpcje = (Button) findViewById(R.id.zamknijOpcje);
        zamknijOpcje.setOnClickListener(this);

    }

    private void addOnClickListnerToListView()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Object object = listView.getItemAtPosition(position);
                OpcjeListModel opcjeListModel = (OpcjeListModel) object;
                switch(position)
                {
                    case 0:
                        Toast.makeText(getApplicationContext(),"Wybrano :"+opcjeListModel.getNazwa(),Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        Toast.makeText(getApplicationContext(),"Wybrano :"+opcjeListModel.getNazwa(),Toast.LENGTH_LONG).show();
                        break;
                }




            }
        });
    }


    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.zamknijOpcje:
                Intent mainIntent = new Intent(this,MainActivity.class);
                startActivity(mainIntent);
                finish();
                break;
        }
    }
}
