package com.example.root.program;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by root on 04.11.16.
 */



public class MyListAdapter extends ArrayAdapter<OpcjeListModel> {

    private ArrayList<OpcjeListModel> dataSet;
    Context mContext;




    //Holder na dane
    private static class ViewHolder {
        TextView  nazwa;
        ImageView obraz;
    }



    public MyListAdapter(ArrayList<OpcjeListModel> data, Context context) {
        super(context, R.layout.opcjeitem_layout, data);
        this.dataSet = data;
        this.mContext=context;

    }





/*
@Description Zwraca "wygląd" Listy
 */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        OpcjeListModel opcjeListModel = getItem(position);

        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.opcjeitem_layout, parent, false);
            viewHolder.nazwa = (TextView) convertView.findViewById(R.id.textListItem);;
            viewHolder.obraz = (ImageView) convertView.findViewById(R.id.imageListItem);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }



        viewHolder.nazwa.setText(opcjeListModel.getNazwa());
        viewHolder.obraz.setTag(position);

        return convertView;
    }


}
