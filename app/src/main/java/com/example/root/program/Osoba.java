package com.example.root.program;

/**
 * Created by root on 08.11.16.
 */

public class Osoba {

    private Integer id;
    private String caleImie;
    private String ulica;
    private String miejoscowosc;
    private String kodPocztowy;

    public Osoba(Integer id, String caleImie,String miejoscowosc, String ulica, String kodPocztowy) {
        this.id = id;
        this.caleImie = caleImie;
        this.kodPocztowy = kodPocztowy;
        this.miejoscowosc = miejoscowosc;
        this.ulica = ulica;
    }

    public  Osoba() {}
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaleImie() {
        return caleImie;

    }

    public void setCaleImie(String caleImie) {
        this.caleImie = caleImie;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getMiejoscowosc() {
        return miejoscowosc;
    }

    public void setMiejoscowosc(String miejoscowosc) {
        this.miejoscowosc = miejoscowosc;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }
}
